#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define N 10 
#define L 100 
#define K 3 

int main()
 {
	int last; 
	int i; 
	char str[N][L];
	int flag[N] = { 0 }; 
	puts("Enter your strings:\n");
	for (i = 0; i < N; i++)
		 {
		
		fgets(str[i], L, stdin);
		flag[i] = 1;
		if (str[i][0] == '\n')
			 {
			last = i - 1;
			flag[i] = 0;
			break;
			}
		if (i == N - 1)
			 {
			last = i;
			break;
			}
		}
	puts("\nRandom string's location\n");
	int ent = 0; 
	srand(time(0));
	while (ent < last - K)
		{
		i = rand() % last + 1;
		if (flag[i] == 1)
			 {
			fputs(str[i], stdout);
			ent++;
			flag[i] = 0;
			}
		}
	for (i = 0; i <= last; i++)
		 {
		if (flag[i] == 1)
			fputs(str[i], stdout);
		}
	return 0;
}